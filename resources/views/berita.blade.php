@extends('temp.template')
@section('content')

    <section class="heroberita"
        style="background-image: linear-gradient(rgba(160, 151, 151, 0.5),rgba(161, 142, 142, 0.5)),url(../assets/img/HEADLINE.png);"
        data-aos="fade-up">
        <div class="col-lg-6 col-md-6 col-sm-6 col-6 text-center herotentang-text_berita box">
            <!-- <h1>Kerjasama Prama <span>Dengan MNC Group</span></h1> -->
        </div>
    </section>

    <section class="news">
        <div class="testimonials_berita">
            <div class="container" data-aos="zoom-in">
                <div class="owl-carousel testimonials-carousel">
                    <div class="testimonial-item">
                        <div class="row">
                            @foreach ($data as $item)
                                <div class="col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                                    <div class="member">
                                        <div class="member-img">
                                            <img src="assets/img/team/team-1.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="berita_box col-lg-12 col-md-12 text-center berita_box">
                                            <h4>{{ $item->nama }}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
